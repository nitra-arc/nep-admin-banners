# BunnersBundle

## Описание

данный бандл предназначен для:

* **создания и редактирования баннеров**

## Подключение

* **composer.json**

```json
{
    ...   
    "require": {
        ...
        "nitra/e-commerce-admin-bannersbundle": "dev-master",
        ...
    }
    ...
}
```

* **app/AppKernel.php**

```php
<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    //...
    public function registerBundles()
    {
        //...
        $bundles = array(
            //...
            new Nitra\BannersBundle\NitraBannersBundle(),
            //...
        );
        //...
        return $bundles;
    }
    //...
}
```