<?php

namespace Nitra\BannersBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ODM\Document
 */
class Banner
{
    use \Gedmo\Blameable\Traits\BlameableDocument;
    use \Nitra\StoreBundle\Traits\LocaleDocument;

    /**
     * @ODM\Id(strategy="AUTO")
     */
    private $id;

    /**
     * @ODM\String
     * @Gedmo\Translatable
     * @Assert\NotBlank
     * @Assert\Length(max = 255)
     */
    private $name;

    /**
     * изображение
     * @ODM\String
     */
    private $image;

    /**
     * flash файл
     * @ODM\String
     */
    private $flash;

    /**
     * статус
     * @ODM\Boolean
     */
    private $isActive;
    
    /**
     * магазины
     * @ODM\ReferenceMany(targetDocument="Nitra\StoreBundle\Document\Store")
     */
    private $stores;

    /**
     * @ODM\ReferenceOne(targetDocument="Nitra\ProductBundle\Document\Category")
     */
    private $locationCategory;
    
    /**
     * @ODM\String
     */
    private $locationLink;
    
    /**
     * @ODM\String
     */
    private $locationType;

    /**
     * @ODM\String
     * @Assert\Length(max = 255)
     */
    private $href;

    /**
     * @ODM\Int
     * @Assert\Type(type="integer") 
     * @Assert\GreaterThanOrEqual(0)
     */
    private $sortOrder;

    public function __construct()
    {
        $this->stores = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return self
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * Get image
     *
     * @return string $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set flash
     *
     * @param string $flash
     * @return self
     */
    public function setFlash($flash)
    {
        $this->flash = $flash;
        return $this;
    }

    /**
     * Get flash
     *
     * @return string $flash
     */
    public function getFlash()
    {
        return $this->flash;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return self
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean $isActive
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Add store
     *
     * @param Nitra\StoreBundle\Document\Store $store
     */
    public function addStore(\Nitra\StoreBundle\Document\Store $store)
    {
        $this->stores[] = $store;
    }

    /**
     * Remove store
     *
     * @param Nitra\StoreBundle\Document\Store $store
     */
    public function removeStore(\Nitra\StoreBundle\Document\Store $store)
    {
        $this->stores->removeElement($store);
    }

    /**
     * Get stores
     *
     * @return Doctrine\Common\Collections\Collection $stores
     */
    public function getStores()
    {
        return $this->stores;
    }

    /**
     * Set href
     *
     * @param string $href
     * @return self
     */
    public function setHref($href)
    {
        $this->href = $href;
        return $this;
    }

    /**
     * Get href
     *
     * @return string $href
     */
    public function getHref()
    {
        return $this->href;
    }

    /**
     * Set sortOrder
     *
     * @param int $sortOrder
     * @return self
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return int $sortOrder
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set locationCategory
     *
     * @param Nitra\ProductBundle\Document\Category $locationCategory
     * @return self
     */
    public function setLocationCategory(\Nitra\ProductBundle\Document\Category $locationCategory)
    {
        $this->locationCategory = $locationCategory;
        return $this;
    }

    /**
     * Get locationCategory
     *
     * @return Nitra\ProductBundle\Document\Category $locationCategory
     */
    public function getLocationCategory()
    {
        return $this->locationCategory;
    }

    /**
     * Set locationLink
     *
     * @param string $locationLink
     * @return self
     */
    public function setLocationLink($locationLink)
    {
        $this->locationLink = $locationLink;
        return $this;
    }

    /**
     * Get locationLink
     *
     * @return string $locationLink
     */
    public function getLocationLink()
    {
        return $this->locationLink;
    }

    /**
     * Set locationType
     *
     * @param string $locationType
     * @return self
     */
    public function setLocationType($locationType)
    {
        $this->locationType = $locationType;
        return $this;
    }

    /**
     * Get locationType
     *
     * @return string $locationType
     */
    public function getLocationType()
    {
        return $this->locationType;
    }
}
