$(document).ready(function(){
    // скрываем все поля
    hideChoiceType();
    // отображаем в зависимости от выбранного типа
    changeType($('.field_locationType select option:selected'));
    // переключение типа
    $('.field_locationType select').change(function(){
        changeType($(this));
    })
    
});

function hideChoiceType() {
    $('.field_locationLink, .field_nl_tree_category').hide();
}

function changeType(_this) {
//    console.log(_this.val())
    if (_this.val() == 'category') {
        $('.field_locationLink').hide();
        $('.field_nl_tree_category').show();
    } else if (_this.val() == 'link') {
        $('.field_locationLink').show();
        $('.field_nl_tree_category').hide();
    } else {
        hideChoiceType();
    }
}